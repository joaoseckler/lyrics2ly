from django import template
from django.utils.safestring import mark_safe
from tagform import tagform_register
from ..forms import Lyrics2LyForm
from ..lyrics2ly import lyrics2ly

register = template.Library()

def process(options):
    l = lyrics2ly(options["text"], options["language"])
    return mark_safe(
        f'<div class="clipboard-copy"><pre><code>{l}</code></pre><div>'
    )

tagform_register(
    library=register,
    tag_name="lyrics2ly_form",
    Form=Lyrics2LyForm,
    process=process,
    template="pages/form.html",
    takes_context=False
)
