import pyphen
import sys
import argparse


def parse():
    parser = argparse.ArgumentParser(
        prog="lyrics2ly",
        description="Hyphenate text to help writing lyrics in lilypond",
    )

    parser.add_argument(
        "-l",
        "--language",
        help="Select language for the hyphenation engine",
        default="en",
    )

    parser.add_argument(
        "--list-languages",
        help="list available languages",
        action="store_true",
    )

    return parser.parse_args()

def hyphenate_hof(dic):
    def hyphenate(word):
        if '-' in word:
            parts = word.split('-')
            return hyphenate(parts[0]) + '\\-' + hyphenate("".join(parts[1:]))

        return dic.inserted(word, ' -- ')
    return hyphenate

def list_languages():
    return list(pyphen.LANGUAGES.keys())

def lyrics2ly(text, lang):
    lines = []
    dic = pyphen.Pyphen(lang=lang)

    for line in text.split("\n"):
        space_beg = len(line) - len(line.lstrip())

        words = map(hyphenate_hof(dic), line.split())
        lines.append(" " * space_beg + ' '.join(words))

    return "\n".join(lines)

if __name__ == "__main__":
    options = parse()

    if options.list_languages:
        print(*list_languages(), sep="\n")
        exit()

    for line in sys.stdin:
        print(lyrics2ly(line, options.language))
