from django.apps import AppConfig


class Lyrics2LyConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "lyrics2ly"
