# Lyrics to `lilypond`

`lyrics2ly` is a little script to help writing lyrics in `lilypond`.
Given some text via standard input, it will simply:

 1. Break all words into syllables, separating them with double hyphens;
 2. Escape existing single hyphens with a backslash.

This is useful for aligning lyrics with music in `lilypond`, but is not
sufficient. Melismas and elisions should be handled manually. For more
information on aligning lyrics and music with `lilypond`, see
[this documentation page](https://lilypond.org/doc/v2.23/Documentation/learning/aligning-lyrics-to-a-melody).

## Web version

A web version for this script is available at
[jseckler.xyz/lyrics2ly](https://jseckler.xyz/lyrics2ly).

## Language selection

You can use the option `--language` or `-l` to choose the language to be
used by the hyphenation engine.

Available languages may depend on the currently installed `hunspell`
hyphenation dictionaries. Use the option `--list-languages` to list
available languages. On debian based systems, for example, if pyphen is
installed with the `python3-pyphen` package, use `sudo apt install
hypen-<desired language>` to install said dictionaries.

## Dependencies

- `pip install pyphen`, `sudo apt install python3-pyphen` or equivalent.
- `sudo apt install hyphen-<desired language>` for desired language
  hyphenation dictionary (not needed if pyphen is installed via `pip`).

## Usage as django app

The other `.py` files besides `lyrics2ly.py` are used to integrate this
script in a django project. This project is live
[here](https://jseckler.xyz/lyrics2ly) and it's code can be found
[here](https://gitlab.com/joaoseckler/website). It defines a simple
two-fielded form and uses another app called
[`tagform`](https://gitlab.com/joaoseckler/tagform) to turn that form
into a templatetag. Usage is simple:

1. Add [`tagform`](https://gitlab.com/joaoseckler/tagform) to your
   project
2. After `{% load lyrics2ly %}`, use the `{% lyrics2ly_form %}` in some
   template.
3. Style the form as needed.
