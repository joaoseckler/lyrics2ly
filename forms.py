from django.conf import settings, global_settings
from django import forms
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError

from .lyrics2ly import list_languages

MAX_LENGTH = getattr(settings, "LYRICS2LY_MAX_LENGTH", 10_000_000)

def code2lang(code):
    lang, *country = code.split("_")
    country = " ".join(country)

    name = next(
        (n for n in global_settings.LANGUAGES if lang == n[0]),
        ("", lang)
    )[1]

    name = _(name).lower()
    if country:
        name += f" ({country.lower()})"

    return name

def label_lang(code):
    return code, code2lang(code)

def order_choices(choices):
    preferred = "pt_BR", "en_US", "es", "fr", "de"
    choices1, choices2 = [], []

    for o in choices:
        if o[0] in preferred:
            choices1.append(o)
        else:
            choices2.append(o)

    choices1.sort(key=lambda o: preferred.index(o[0]))

    return (
        ("\u2014", tuple(choices1)),
        ("\u2014", tuple(choices2)),
    )

class Lyrics2LyForm(forms.Form):
    text = forms.CharField(
        label=_("lyrics"),
        widget=forms.Textarea(attrs={"placeholder": "…"}),
        max_length=MAX_LENGTH,
        required=False,
    )

    language = forms.ChoiceField(
        label=_("language"),
        choices = order_choices(map(label_lang, list_languages())),
    )

    def clean_text(self):
        text = self.cleaned_data['text']
        if not text:
            raise ValidationError(_("Please convert something non-empty"))
        return text
